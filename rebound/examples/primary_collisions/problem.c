/* Integrator code edited by Saif Charaniya
Takes system number as input
Requires sorted.csv file to initialize planetary system
Integrates systems until the first collision is observed
Outputs heliocentric coordinates and velocity vectors
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <time.h>
#include "main.h"
#include "tools.h"
#include "output.h"
#include "particle.h"
#include "integrator.h"
#include "collision_resolve.h"
#include <string.h>

#ifdef OPENGL
extern int display_wire;
extern int display_spheres;
#endif // OPENGL

int collided = 0;   //exit integration condition

double p_mass[7];   //mass
double p_sma[7];    //semi major axis (sma)    
double p_ma[7];     //mean anomaly
int system_number;

// Define our own collision resolve function, which will only record collisions but not change any of the particles.		
void collision_record_only(struct collision c){
	double delta_t = 2.*M_PI; 					// 1 year	
	
	// only record a maximum of one collision per year per particle
	if ( particles[c.p1].lastcollision+delta_t < t  &&  particles[c.p2].lastcollision+delta_t < t ){

        printf("\nA collision has occured!");
		particles[c.p1].lastcollision = t; 
		particles[c.p2].lastcollision = t;
		FILE* of = fopen("collisions_hydrogen.txt","a+");		// open file for collision output
        fprintf(of, "%i\t", system_number);             //system number for reference
		fprintf(of, "%e\t",t);					        // time
		fprintf(of, "%e\t",(particles[c.p1].x-particles[c.p2].x));	// x position
		fprintf(of, "%e\t",(particles[c.p1].y-particles[c.p2].y));	// y position
		fprintf(of, "%e\t",(particles[c.p1].z-particles[c.p2].z));	// x position
		fprintf(of, "%e\t",(particles[c.p1].vx-particles[c.p2].vx));	// y position
		fprintf(of, "%e\t",(particles[c.p1].vy-particles[c.p2].vy));	// x position
		fprintf(of, "%e\t",(particles[c.p1].vz-particles[c.p2].vz));	// y position
		fprintf(of, "\n");
		fclose(of);		
        collided = 1;				// close file
	}
}

void total_energy(){
	double energy = 0;
	for (int i=0;i<N;i++){
		struct particle p1 = particles[i];
		energy += 0.5*p1.m*(p1.vx*p1.vx + p1.vy*p1.vy + p1.vz*p1.vz);
		for (int j=i+1;j<N;j++){
			struct particle p2 = particles[j];
			double dx = p2.x - p1.x;
			double dy = p2.y - p1.y;
			double dz = p2.z - p1.z;
			energy -= G*p1.m*p2.m/sqrt(dx*dx + dy*dy + dz*dz + softening*softening);
		}
	}

    FILE* of = fopen("energy.txt","a+");		// open file for energy output
    fprintf(of, "%e\t", energy);					// energy
	fprintf(of, "\n");
	fclose(of);
}


//read data for the system from sorted.csv
//system number begins at 1, ends at number of systems
void get_data(int system_number){

    FILE* sorted = fopen("sorted.csv", "r");
    
    //read to current line
    int l;
    char line[1024];
    for (l = 1; l < system_number; l++){
        fgets(line, sizeof(line), sorted);
    }

    //read system line
    fgets(line, sizeof(line), sorted);
    
    //split string token
    char* tokens = strtok(line, " ");
    
    //get time (but not needed really)
    tokens = strtok(NULL, " ");
    
    //get mass
    int i;
    for (i = 0; i < 7; i++){
        
        p_mass[i] = atof(tokens);
        tokens = strtok(NULL, " ");    
    }

    //get sma
    for (i = 0; i < 7; i++){
        
        p_sma[i] = atof(tokens);
        tokens = strtok(NULL, " ");
    }
}

void problem_init(int argc, char* argv[]){
	
    dt = pow(0.1 ,3./2.)*0.1*2.*M_PI;				// initial timestep

    //get args and set data
    system_number = atoi(argv[1]);
    printf("system number %i", system_number);
    get_data(system_number);
    make_random_data();

#ifdef OPENGL
	display_wire	= 1;						// show instantaneous orbits
	display_spheres = 0;						// don't show spheres
#endif // OPENGL
	init_boxwidth(10);

	collision_resolve = collision_record_only;			// Set function pointer for collision recording.

    //create host star
	struct particle star;
	star.m = 1;
	star.r = 7.0e10/1.5e13;				//star has a radius	
	star.x = 0; 	star.y = 0; 	star.z = 0;
	star.vx = 0; 	star.vy = 0; 	star.vz = 0;
	particles_add(star);
	
	// Add planets
	int N_planets = 7;
	double planetmass[8];
	planetmass[0]=0;

	for (int i=0;i<N_planets;i++){
		struct particle planet;
		
        planet.m = 3e-6 * p_mass[i];
		planetmass[i+1] = planet.m;
		semimajor[i] = p_sma[i];
		
        double a = p_sma[i];
		double v = sqrt(1./a); 			       // velocity (circular orbit), in unit of 2pi*AU/yr = 31.4 km/s
        
        planet.r = (2.0 / 3.0) * p_mass[i] * (6.4e8/ 1.5e13);
		planet.lastcollision = 0; 
		planet.x = a; 	planet.y = 0; 	
		double heating = 2.0/180.0*3.1415926/50.; 	// a spread in inclination, somehow need to divide a factor of 50
		// maybe normalization issue
		planet.z = tools_normal(heating)*a;	//give some random z
		planet.vx = 0; 	planet.vy = v; 	
		planet.vz = 0;	//random vz
		particles_add(planet); 
		//
		printf("planet %i: mass %f, a=%f,radius %f R_E, v_esc=%f km/s.\n",i,planet.m/3.0e-6,p_sma[i],planet.r*1.5e13/6.4e8,sqrt(6.67e-8*planet.m*6.0e27/(planet.r*1.5e13)/1.e5));
	}
	tools_move_to_center_of_momentum();				// This makes sure the planetary systems stays within the computational domain and doesn't drift.

        //cal and save initial energy
        //total_energy();
}



void problem_inloop(){
}

void problem_output(){
	if (output_check(10.*2.*M_PI)){  
		output_timing();
		//output_append_orbits("output_el.txt");
		//output_append_ascii("output_xy.");
	}
    if (collided == 1){
        //end integration
        tmax = t;
    }
    if (output_check(2*M_PI)){
        //total_energy();
    }
    
}

void problem_finish(){
    
    //total_energy();
}
