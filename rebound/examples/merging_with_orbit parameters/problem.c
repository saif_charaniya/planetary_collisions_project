/*
Merging planetary systems code, with changes done by Saif Charaniya
This code reads in a system from a csv file, integrates the system until all possible 
collisions have taken place.  Collisions between particles result in mergers with the same density.
Orbital parameters are outputed to a txt file.
*/


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <time.h>
#include "main.h"
#include "tools.h"
#include "output.h"
#include "particle.h"
#include "integrator.h"
#include "collision_resolve.h"
#include <string.h>


#ifdef OPENGL
extern int display_wire;
#endif // OPENGL

double p_mass[7];   //mass
double p_sma[7];    //semi major axis (sma)    
int system_number = 0;
int collision_number = 0;  //from 1 - 6
char orbit_file[240];


void collision_resolve_merger(struct collision c);

///output:
void append_orbits_only(char* filename){
#ifdef MPI
	char filename_mpi[1024];
	sprintf(filename_mpi,"%s_%d",filename,mpi_id);
	FILE* of = fopen(filename_mpi,"a"); 
#else // MPI
	FILE* of = fopen(filename,"a"); 
#endif // MPI
	if (of==NULL){
		printf("\n\nError while opening file '%s'.\n",filename);
		return;
	}
	struct particle com = particles[0];
	for (int i=1;i<N;i++){
		struct orbit o = tools_p2orbit(particles[i],com);
		fprintf(of,"%e\t%i\t%e\t%e\t%e\t%e\t%e\t%e\n",t,i,o.a,o.e,o.inc,o.Omega,o.omega,o.P);
		com = tools_get_center_of_mass(com,particles[i]);
	}
	fclose(of);
}



//read data for the system from sorted.csv
//system number begins at 1, ends at number of systems
void get_data(int system_number){

    FILE* sorted = fopen("sorted.csv", "r");
    
    //read to current line
    int l;
    char line[1024];
    for (l = 1; l < system_number; l++){
        fgets(line, sizeof(line), sorted);
    }

    //read system line
    fgets(line, sizeof(line), sorted);
    
    //split string token
    char* tokens = strtok(line, " ");
    
    //get time (but not needed really)
    tokens = strtok(NULL, " ");
    
    //get mass
    int i;
    for (i = 0; i < 7; i++){
        
        p_mass[i] = atof(tokens);
        tokens = strtok(NULL, " ");    
    }

    //get sma
    for (i = 0; i < 7; i++){
        
        p_sma[i] = atof(tokens);
        tokens = strtok(NULL, " ");
    }
}

void problem_init(int argc, char* argv[]){
	dt = pow(0.1 ,3./2.)*0.1*2.*M_PI;				// initial timestep

#ifdef OPENGL
	display_wire	= 1;						// show instantaneous orbits
#endif // OPENGL
	collision_resolve = collision_resolve_merger;			// Setup our own collision routine.
	init_boxwidth(10); 		

    //get args and set data
    system_number = atoi(argv[1]);
    collision_number = 1;
    printf("system number %i", system_number);
    get_data(system_number);
    sprintf(orbit_file, "orbits_system_%i", system_number);			

    struct particle star;
	star.m = 1;
	star.r = 7.0e10/1.5e13;				//star has a radius	
	star.x = 0; 	star.y = 0; 	star.z = 0;
	star.vx = 0; 	star.vy = 0; 	star.vz = 0;
	particles_add(star);
	
	// Add planets
	int N_planets = sizeof(p_mass)/sizeof(p_mass[0]);

	for (int i=0;i<N_planets;i++){
		struct particle planet;
		
        planet.m = 3e-6 * p_mass[i];
        double a = p_sma[i];
		double v = sqrt(1./a); 			       // velocity (circular orbit), in unit of 2pi*AU/yr = 31.4 km/s
        planet.r = (1.0 / 3.0) * p_mass[i] * (6.4e8/ 1.5e13);
		planet.lastcollision = 0; 
		planet.x = a; 	planet.y = 0; 	
		double heating = 2.0/180.0*3.1415926/50.; 	// a spread in inclination, somehow need to divide a factor of 50
		// maybe normalization issue
		planet.z = tools_normal(heating)*a;	//give some random z
		planet.vx = 0; 	planet.vy = v; 	
		planet.vz = 0;	//random vz
		particles_add(planet); 
		//
		printf("planet %i: mass %f, a=%f,radius %f R_E, v_esc=%f km/s.\n",i,planet.m/3.0e-6,  a,planet.r*1.5e13/6.4e8,sqrt(6.67e-8*planet.m*6.0e27/(planet.r*1.5e13)/1.e5));
	}

	tools_move_to_center_of_momentum();				// This makes sure the planetary systems stays within the computational domain and doesn't drift.
    output_append_orbits_only(orbit_file);
}

void collision_print(struct collision c)
{
    printf("\nA collision has occured!");
	particles[c.p1].lastcollision = t; 
	particles[c.p2].lastcollision = t;
	FILE* of = fopen("collisions.txt","a+");		// open file for collision output
    fprintf(of, "%i\t", system_number);             //system number for reference
    fprintf(of, "%i\t", collision_number);          //colllision number (max is 6 for 7 planets)
	fprintf(of, "%e\t",t);					        // time
	fprintf(of, "%e\t",(particles[c.p1].x-particles[c.p2].x));	// x position
	fprintf(of, "%e\t",(particles[c.p1].y-particles[c.p2].y));	// y position
	fprintf(of, "%e\t",(particles[c.p1].z-particles[c.p2].z));	// x position
	fprintf(of, "%e\t",(particles[c.p1].vx-particles[c.p2].vx));	// y position
	fprintf(of, "%e\t",(particles[c.p1].vy-particles[c.p2].vy));	// x position
	fprintf(of, "%e\t",(particles[c.p1].vz-particles[c.p2].vz));	// y position
	fprintf(of, "\n");
	fclose(of);		
}

void collision_resolve_merger(struct collision c){
	struct particle p1 = particles[c.p1];
	struct particle p2 = particles[c.p2];
	double x21  = p1.x  - p2.x; 
	double y21  = p1.y  - p2.y; 
	double z21  = p1.z  - p2.z; 
	double rp   = p1.r+p2.r;
	//printf("collision 1\n");
	if (rp*rp < x21*x21 + y21*y21 + z21*z21) return; // not overlapping
	double vx21 = p1.vx - p2.vx; 
	double vy21 = p1.vy - p2.vy; 
	double vz21 = p1.vz - p2.vz; 
	//printf("collision 2\n");
	if (vx21*x21 + vy21*y21 + vz21*z21 >0) return; // not approaching
	printf("collision 3 %f %f %f\n", p1.lastcollision, p2.lastcollision, t);
	if (p1.lastcollision>=t || p2.lastcollision>=t) return; // already collided
	//printf("collision 4\n");
	particles[c.p2].lastcollision = t;
	particles[c.p1].lastcollision = t;
	// Note: We assume only one collision per timestep. 
	// Setup new particle (in position of particle p1. Particle p2 will be discarded.
    collision_print(c);
	struct particle cm = tools_get_center_of_mass(p1, p2);
	particles[c.p1].x = cm.x;
	particles[c.p1].y = cm.y;
	particles[c.p1].z = cm.z;
	particles[c.p1].vx = cm.vx;
	particles[c.p1].vy = cm.vy;
	particles[c.p1].vz = cm.vz;
	particles[c.p1].r = p1.r*pow(cm.m/p1.m,1./3.);	// Assume a constant density
	particles[c.p1].m = cm.m;
	// Remove one particle.
	N--;
	particles[c.p2] = particles[N];

    collision_number++;
	// Make sure we don't drift, so let's go back to the center of momentum
	tools_move_to_center_of_momentum();	
    //output_orbits("orbits.txt");
    output_append_orbits_only(orbit_file);
}

void problem_inloop(){
}

void problem_output(){
	if (output_check(10.*2.*M_PI)){  
     //		output_append_orbits();
        output_timing(); 
        //output_append_orbits("orbits.txt");
        output_append_orbits_only(orbit_file);
	}
    if (collision_number == 2){
        //tmax = t;
    }
}

void problem_finish(){
}
