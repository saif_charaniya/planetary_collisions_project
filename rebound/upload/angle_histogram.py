#saif charaniya
#plot histogram of angles

import numpy as np
import pylab as pl
from scipy.optimize import curve_fit

normal = np.genfromtxt("../final_normal_output.csv", delimiter = ",",
                    usecols = (4), skip_header = 1)

#add hydrogren
hydro = np.genfromtxt("../final_hydrogen_output.csv", delimiter = ",",
                    usecols = (4), skip_header = 1)

#get vars
a_n = normal * 57.2957795
a_h = hydro * 57.2957795


#combine all data sources
a_f = np.append(a_n, a_h)

#plotting      
#plot histogram
pl.figure()
n, bins, patches = pl.hist(a_f, 18, normed=True, histtype="stepfilled")
pl.setp(patches, "facecolor", "b", "alpha", 0.75)
mean = np.mean(a_f)
variance = np.var(a_f)
sigma = np.sqrt(variance)
msg = "mean = %.2f\nmedian = %.2f" % (mean, np.median(a_f))
pl.plot(bins, pl.normpdf(bins,mean,sigma), "k--")
pl.title("Impact Angle Distribution")
pl.xlabel("Angle [Degrees]")
#pl.text(2.2, .02, msg)                                                  
#pl.savefig("angle_hist.png")
print(msg)
pl.show()
print ("done")
