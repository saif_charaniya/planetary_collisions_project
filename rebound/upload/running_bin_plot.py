#saif charaniya
#running bin plot with linear fit

import numpy as np
import pylab as pl
import linear as linear

#read file
hydro = np.genfromtxt("../final_hydrogen_output.csv", delimiter = ",",
                    usecols = (2, 3, 5), skip_header = 1)

normal = np.genfromtxt("../final_normal_output.csv", delimiter = ",",
                    usecols = (2, 3, 5), skip_header = 1)

#get vars
vesc_n = normal[:,[0][0]]
v_n = normal[:,[1][0]]
k_n = normal[:,[2][0]]

vesc_h = hydro[:,[0][0]]
v_h = hydro[:,[1][0]]
k_h = hydro[:,[2][0]]

dv_n = v_n / vesc_n
f = np.copy(dv_n)
dv_h = v_h / vesc_h

#append
dv_n = np.append(dv_n, dv_h)
k_n = np.append(k_n, k_h)

#binning process
# change I to change bin width
I = 1.0
N = int(8 / I)
print ("vars:\n")
print ("I = ", I)

#iterate through bins 5 - 10 ish
bin_heights_mean = []
bin_heights_med = []
bin_center_avg = []
bin_center_mean = []
bin_center_med = []
bin_heights_mean_se = []
bin_heights_med_se = []
bin_center_med_se = []
bin_center_mean_se = []
bin_sizes = []

s = min(k_n)
e = s + I

while (e < 10.0):

    index_a = np.where(k_n >= s)[0]
    index_b = np.where(k_n <= e)[0]
    index_f = np.intersect1d(index_a, index_b)

    ks = k_n[index_f]
    vs = dv_n[index_f]

    bin_sizes.append(len(ks))
    bin_heights_mean.append(np.mean(vs))
    bin_heights_med.append(np.median(vs))
    bin_center_avg.append((s + e) / 2)
    bin_center_mean.append(np.mean(ks))
    bin_center_med.append(np.median(ks))
    bin_center_mean_se.append(np.std(ks) / np.sqrt(len(ks)))
    bin_heights_mean_se.append(np.std(vs) / np.sqrt(len(vs)))
    bin_center_med_se.append(1.253 * np.std(ks) / np.sqrt(len(ks)))
    bin_heights_med_se.append(1.253 * np.std(vs) / np.sqrt(len(vs)))
    
    s = s + I / 2
    e = s + I


#linear fitting:
xvar1, yvar1, m1, c1, em1, ec1 = linear.lsq(np.copy(bin_center_mean), np.array(bin_heights_mean))
fitted1 = xvar1 * m1 + c1

xvar2, yvar2, m2, c2, em2, ec2 = linear.lsq(np.copy(bin_center_med), np.array(bin_heights_med))
fitted2 = xvar2 * m2 + c2  

f1 = pl.figure()
ax1 = f1.add_subplot(111)
ax1.errorbar(bin_center_mean, bin_heights_mean, fmt="ro",\
            yerr=bin_heights_mean_se, xerr=bin_center_mean_se)
ax1.plot(bin_center_mean, fitted1,label="mean")
ax1.set_xlabel("k")
ax1.set_ylabel("$V_{imp}$ / $V_{esc}$")
ax1.set_title("Binned Linear Fit")
#text1 = "m = " + str(m1) + " +- " + str(em1) + "\nc = " + str(c1) + " +- " + str(ec1) 
#ax1.text(6.5, 1.10, text1)

ax1.errorbar(bin_center_med, bin_heights_med, fmt="ro",\
            yerr=bin_heights_med_se, xerr=bin_center_med_se)
ax1.plot(bin_center_med, fitted2, label="median")
#text2 = "m = " + str(m2) + " +- " + str(em2) + "\nc = " + str(c2) + " +- " + str(ec2)
#ax2.text(6.5, 1.05, text2)
pl.legend(loc=0)
#pl.savefig("binning.png")
pl.show()

print ("done")
