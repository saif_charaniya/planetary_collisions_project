#make historgrams for velocity
import numpy as np
import pylab as pl

file = "../multi_collision_output.csv"

collisions = np.genfromtxt(file, delimiter = ",", \
    skip_header = 3, usecols = (1,4))

col_num = collisions[:,[0][0]]
v = collisions[:,[1][0]]

#sort v
v_esc = 1.936420000000000030E1
v1 = v[np.where(col_num == 1)[0]] / v_esc
v2 = v[np.where(col_num == 2)[0]] / v_esc
v3 = v[np.where(col_num == 3)[0]] / v_esc
v4 = v[np.where(col_num == 4)[0]] / v_esc
v5 = v[np.where(col_num == 5)[0]] / v_esc
v6 = v[np.where(col_num == 6)[0]]  / v_esc

all_v = [v1, v2, v3, v4, v5, v6]
means = []
medians = []
stds = []
col = range(1,6)

#plot histogram and stats
for i in range(5):
##    pl.figure()
    dv_n = all_v[i]
##    n, bins, patches = pl.hist(dv_n, 25, normed=True, histtype="stepfilled")
##    pl.setp(patches, "facecolor", "b", "alpha", 0.75)
    means.append(np.mean(dv_n))
    medians.append(np.median(dv_n))
    stds.append(np.std(dv_n))
##    med = np.median(dv_n)
##    variance = np.var(dv_n)
##    sigma = np.sqrt(variance)
##    msg = "mean = " + str(mean) + "\nmedian = " + str(med) \
##           + "\nstd = " + str(sigma)
##    print (str(i + 1) + msg)
##    pl.plot(bins, pl.normpdf(bins,mean,sigma), "k--")
##    pl.title("Impact Velocity Collision " + str(i + 1))
##    pl.xlabel("Impact Velocity / Escape Velocity")
##    pl.text(2, 1, msg)
##    pl.savefig("collision_" + str(i + 1) + "_histogram.png")
##    pl.show()

pl.figure()
pl.title("Collision Impact Velocity Ratios")
pl.xlabel("Collision Number")
pl.ylabel("$V_{imp}$ / $V_{esc}$")
pl.errorbar(col, means, fmt="ro-", label="mean")
pl.errorbar(col, medians, fmt="bo-", label="median")
pl.legend(loc=0)
pl.xticks(range(7))
#pl.savefig("../../final_plots_only/linear_multi.png")
pl.show()


    
