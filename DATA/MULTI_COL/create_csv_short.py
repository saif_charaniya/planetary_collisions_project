#saif charaniya
#create csv file for "collisions_sc_normal.txt"

#import statements
import numpy as np

#read files
bonan_file = np.genfromtxt("Bonan_FlatCoplanar.csv", delimiter = ",", \
skip_header = 3, usecols = (3, 4, 5))

collision_file = np.genfromtxt("multi_collisions_fix.txt")

#set vars
system_num = collision_file[:,[0][0]]
system_num = np.array(system_num, dtype=int)
collision_number = collision_file[:,[1][0]]
time = collision_file[:,[2][0]]
x = collision_file[:,[3][0]]
y = collision_file[:,[4][0]]
z = collision_file[:,[5][0]]
vx = collision_file[:,[6][0]]
vy = collision_file[:,[7][0]]
vz = collision_file[:,[8][0]]

k_mean = bonan_file[:,[0][0]][system_num - 1]
k_min = bonan_file[:,[1][0]][system_num - 1]
k_std = bonan_file[:,[2][0]][system_num - 1]

#convert au to km and 2p au /yr to km/s
vx = vx * 2 * np.pi * 4.7619 
vy = vy * 2 * np.pi * 4.7619
vz = vz * 2 * np.pi * 4.7619

x = x * 1.5e8
y = y * 1.5e8
z = z * 1.5e8

dot = (x * vx + y * vy + z * vz)

dx = np.sqrt(x ** 2 + y ** 2 + z ** 2) #au
dv = np.sqrt(vx ** 2 + vy ** 2 + vz ** 2) #km/s

cos_a = abs(dot / (dx * dv))
a = np.arccos(cos_a) #radians

v_esc = np.repeat([19.3642], len(x))

#save to csv file
save_file = "multi_collision_output.csv"
save_file_header = "system_number,collisions_number,collision_time,v_esc,v,angle,k_mean,k_min,k_std"
out_vals = (system_num, collision_number, time, v_esc, dv, a, k_mean, k_min, k_std)
np.savetxt(save_file, np.column_stack(out_vals), delimiter = ",",\
           header = save_file_header)

print ("done")
