#write to file the first collision parameters
#only need v and k_mean
import numpy as np

collisions = np.genfromtxt("multi_collision_output.csv", delimiter=","\
                           ,skip_header = 1, usecols = (1,4,6))

col_num  = collisions[:,[0][0]]
v = collisions[:,[1][0]]
k = collisions[:,[2][0]]

indicies = np.where(col_num == 1)[0]

v = v[indicies]
k = k[indicies]

#save
var = (v,k)
np.savetxt("single_collisions,csv", np.column_stack(var), delimiter = ",")

                           
