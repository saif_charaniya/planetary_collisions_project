#saif charaniya
#plot dx and dv against k using csv file

import numpy as np
import pylab as pl

#read file
csv = np.genfromtxt("normal_systems_output.csv", delimiter = ",",
                    usecols = (2, 3, 4, 5, 6), skip_header = 1)


#get vars
dx = csv[:,[0][0]]
dv = csv[:,[1][0]]
k_mean = csv[:,[2][0]]
k_min = csv[:,[3][0]]
k_std = csv[:,[4][0]]

#plot scatter k_mean
pl.figure()
pl.title("dv vs k_mean")
pl.plot(k_mean, dv, "ro", label = "dv")
pl.legend(loc=0)
pl.savefig("dv_vs_kmean.png")

pl.figure()
pl.title("dx vs k_mean")
pl.plot(k_mean, dx, "ro", label = "dx")
pl.legend(loc=0)
pl.savefig("dx_vs_kmean.png")

pl.show()

#plot scatter k_min
pl.figure()
pl.title("dv vs k_min")
pl.plot(k_min, dv, "ro", label = "dv")
pl.legend(loc=0)
pl.savefig("dv_vs_kmin.png")

pl.figure()
pl.title("dx vs k_min")
pl.plot(k_min, dx, "ro", label = "dx")
pl.legend(loc=0)
pl.savefig("dx_vs_kmin.png")

pl.show()

#plot scatter k_std
pl.figure()
pl.title("dv vs k_std")
pl.plot(k_std, dv, "ro", label = "dv")
pl.legend(loc=0)
pl.savefig("dv_vs_kstd.png")

pl.figure()
pl.title("dx vs k_std")
pl.plot(k_std, dx, "ro", label = "dx")
pl.legend(loc=0)
pl.savefig("dx_vs_kstd.png")

pl.show()
print ("done")
