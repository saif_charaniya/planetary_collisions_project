#saif charaniya
#plot dx and dv against k using csv file

import numpy as np
import pylab as pl
import linear as linear

#read file
normal = np.genfromtxt("../final_normal_output.csv", delimiter = ",",
                    usecols = (2, 3, 5), skip_header = 1)

#get vars
vesc_n = normal[:,[0][0]]
v_n = normal[:,[1][0]]
k_n = normal[:,[2][0]]

dv_n = v_n / vesc_n

#binning process
#from k = 5 to k =13 , .5k interval
#= 16 bins
I = 0.5
N = int(8 / I)
print ("vars:\n")
print ("I = ", I)
print ("\nN = ", N)

bins = np.arange(5, 13, I)
bin_vals = []
for i in range(N):
    bin_vals.append([])
    
for i in range(0, len(dv_n)):
    ki = k_n[i]
    l = int((ki - 5) / I)
    bin_vals[l].append(dv_n[i])

#take mean of each bin
bin_means = []
bin_se_mean = []
bin_medians = []
bin_se_median = []
for i in range(N):
    bin_means.append(np.mean(bin_vals[i]))
    bin_se_mean.append(np.std(bin_vals[i]) / np.sqrt(len(bin_vals[i])))
    bin_medians.append(np.median(bin_vals[i]))
    bin_se_median.append(1.253 * bin_se_mean[i])

bin_k = bins + I / 2.0


#linear fitting:
xvar1, yvar1, m1, c1, em1, ec1 = linear.lsq(np.copy(bin_k), np.array(bin_means))
fitted1 = xvar1 * m1 + c1

xvar2, yvar2, m2, c2, em2, ec2 = linear.lsq(np.copy(bin_k), np.array(bin_medians))
fitted2 = xvar2 * m2 + c2


#plot scatter k_mean
pl.figure()
pl.title("Normal Systems bin size = 0.5")
pl.errorbar(bin_k, bin_means, fmt="ro", yerr=bin_se_mean, label="mean")
pl.errorbar(bin_k, bin_medians, fmt="bo", yerr=bin_se_median, label="median")
pl.plot(xvar1, fitted1, "r-")
pl.plot(xvar2, fitted2, "b-")
pl.xlabel("k")
pl.ylabel("Impact Velocity / Escape Velocity")
pl.legend(loc=0)
pl.savefig("normal_binned_05.png")

pl.show()
print ("done")
