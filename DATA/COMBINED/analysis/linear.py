#linear
#to do linear least squares fitting using matrixes
import numpy as np
import math as math

#find m and c
#return errors as well
#Return -> xvar, yvar, m, c ,em, ec
def lsq (xvar, yvar):

    xvar, yvar = nanout(xvar, yvar)
    m, c = mc(xvar, yvar)
    em, ec = err(xvar, yvar, m, c)

    return xvar, yvar, m, c, em, ec

#remove nans
#return xvar and vyar without nans that appear in begining
def nanout(xvar, yvar):

    ni = 0
    for i in yvar:
        if (math.isnan(i)):
            ni+=1

    newx = np.copy(xvar[ni:])
    newy = np.copy(yvar[ni:])

    return newx, newy

#return m and c
def mc(xvar, yvar):

    ma = np.array([[np.sum(xvar ** 2), np.sum(xvar)], [np.sum(xvar), len(xvar)]])
    mc = np.array([[np.sum(xvar * yvar)], [np.sum(yvar)]])
    mai = np.linalg.inv(ma)
    md = np.dot(mai, mc)
        
    m = md[0, 0]
    c = md[1, 0]
    
    return m, c

#return errors in m and c
def err(xvar, yvar, m, c):
    
    n = len(xvar)
    std2 = (1 / (n - 2)) * np.sum((yvar - m * xvar + c)**2)

    em = (n * std2) / ((n * np.sum(xvar ** 2)) - (np.sum(xvar) ** 2))
    ec = (std2 * np.sum(xvar ** 2)) / ((n * np.sum(xvar ** 2))-(np.sum(xvar) ** 2))

    return em, ec

