#saif charaniya
#plot histogram of normal

import numpy as np
import pylab as pl

#read files
normal = np.genfromtxt("../final_normal_output.csv", delimiter = ",",
                    usecols = (2, 3, 5), skip_header = 1)

#add hydrogren
hydro = np.genfromtxt("../final_hydrogen_output.csv", delimiter = ",",
                    usecols = (2, 3, 5), skip_header = 1)

#get vars
vesc_h = hydro[:,[0][0]]
v_h = hydro[:,[1][0]]
k_h = hydro[:,[2][0]]

#get vars
vesc_n = normal[:,[0][0]]
v_n = normal[:,[1][0]]
k_n = normal[:,[2][0]]

dv_n = v_n / vesc_n
dv_h = v_h / vesc_h

#combine all data sources (and maintain sequence)
final_v = np.append(dv_n, dv_h)

#plotting      
#plot histogram
pl.figure()
n, bins, patches = pl.hist(final_v, 50, normed=True, histtype="stepfilled")
pl.setp(patches, "facecolor", "b", "alpha", 0.75)
mean = np.mean(final_v)
variance = np.var(final_v)
sigma = np.sqrt(variance)
msg = "mean   = %.2f\nmedian = %.2f\nstd = %.2f" % (mean, np.median(final_v), sigma)
pl.plot(bins, pl.normpdf(bins,mean,sigma), "k--")
pl.title("Impact Velocity")
pl.xlabel("$V_{imp} / V_{esc}$")
pl.text(2, 4, msg, fontsize=20)                                                  
#pl.savefig("histogram.png")
pl.show()

print ("done")
