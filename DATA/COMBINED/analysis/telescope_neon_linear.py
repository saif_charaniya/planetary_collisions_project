#Quadratic least squares fitting
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
import matplotlib.gridspec as gridspec
from scipy.optimize import leastsq
from matplotlib.ticker import MaxNLocator



rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})

nx = 17 #Number of data points

pixel = np.array([493, 440, 435, 425, 420, 396, 380, 369, 364, 340, 319,
                  300, 242, 217, 168, 107, 76])
wavelength = np.array([585.24879, 587.28275, 588.18952, 594.48342, 596.5471, 
                       602.99969, 607.43377, 614.30626, 616.35939, 621.72812,
                       626.6495, 633.44278, 638.29917, 640.2248, 650.65281,
                       659.89529, 667.82762])
pixel = 765 - pixel
pixel.sort()
wavelength.sort()


# Construct the matrices
ma = np.array([ [np.sum(pixel**2), np.sum(pixel)],[np.sum(pixel), nx ] ] )
mc = np.array([ [np.sum(pixel*wavelength)],[np.sum(wavelength)]])
# Compute the gradient and intercept
mai = np.linalg.inv(ma)
#print 'Test matrix inversion gives identity',np.dot(mai,ma)
md = np.dot(mai,mc) # matrix multiply is dot
# Overplot the best fit
mfit = md[0,0]
cfit = md[1,0]

best_fit = mfit*pixel + cfit
res = wavelength-best_fit

coef = np.polyfit(pixel,wavelength,1)#, full=True)
def peval(p, pixel):
    '''Return index of of function with parameters p and wavelength'''
    return p[0]*pixel + p[1]
def residuals(p, wavelength, pixel):
    '''Return residual at point (wavelength,index)'''
    return wavelength - peval(p, pixel)

p_final, cov_x, info, mesg, success = leastsq(residuals, coef, args=(wavelength,pixel), full_output = True)


gs = gridspec.GridSpec(2, 1,height_ratios=[2,1])
#f, (ax1, ax2) = plt.subplots(2)#, sharex=True,gs)
f = plt.figure()
ax1 = f.add_subplot(gs[0])
f.text(0.5, 0.04, 'Pixel Number', ha='center', va='center')
f.text(0.04, 0.45, 'Wavelength (nm)', ha='center', va='center', rotation='vertical')
ax1.plot(pixel, best_fit)
ax1.plot(pixel, wavelength, "go")
ax1.set_ylim(560,680)
ax1.set_title('Linear Calibration for Telescope CCD')
plt.gca().yaxis.set_major_locator(MaxNLocator(6))

ax2 = f.add_subplot(gs[1],sharex=ax1)
ax2.plot(pixel,res,"go")
ax2.set_title("Residuals")
ax2.set_ylim(-10,8)
plt.gca().yaxis.set_major_locator(MaxNLocator(5))
# Fine-tune figure; make subplots close to each other and hide x ticks for
# all but bottom plot.
f.subplots_adjust(hspace=0.15)
plt.setp([a.get_xticklabels() for a in f.axes[:-1]], visible=False)#, adjustable='datalim')

#plt.savefig('linear_calibration_telescope.png')
plt.show()
