#saif charaniya
#plot histogram of normal

import numpy as np
import pylab as pl
from scipy.optimize import curve_fit

normal = np.genfromtxt("../final_normal_output.csv", delimiter = ",",
                    usecols = (2, 3, 5), skip_header = 1)

#add from single collision merger data
merger = np.genfromtxt("single_collisions,csv", delimiter = ",")

m_v = merger[:,[0][0]]
m_k = merger[:,[1][0]]

#get vars
vesc_n = normal[:,[0][0]]
v_n = normal[:,[1][0]]
k_n = normal[:,[2][0]]

m_esc = vesc_n[0]

dv_m = m_v / m_esc
dv_n = v_n / vesc_n

pl.figure()
#plot histogram
n, bins, patches = pl.hist(dv_n, 25, normed=True, histtype="stepfilled")
pl.setp(patches, "facecolor", "b", "alpha", 0.75)
mean = np.mean(dv_n)
variance = np.var(dv_n)
sigma = np.sqrt(variance)
pl.plot(bins, pl.normpdf(bins,mean,sigma), "k--")
pl.title("Impact Velocity")
pl.xlabel("$V_{imp} / V_{esc}$")
pl.savefig("normal_hist_0.png")

###plot histogram
##n, bins, patches = pl.hist(dv_h, 25, normed=True, histtype="stepfilled")
##pl.setp(patches, "facecolor", "b", "alpha", 0.75)
##mean = np.mean(dv_h)
##variance = np.var(dv_h)
##sigma = np.sqrt(variance)
##pl.plot(bins, pl.normpdf(bins,mean,sigma), "k--")
##pl.title("Impact Velocity")
##pl.xlabel("Impact Velocity / Escape Velocity")
##pl.savefig("hydrogen_hist.png")
##pl.show()
##



