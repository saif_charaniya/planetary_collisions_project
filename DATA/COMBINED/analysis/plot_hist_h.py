#saif charaniya
#plot histogram of normal

import numpy as np
import pylab as pl
from scipy.optimize import curve_fit

hydro = np.genfromtxt("../final_hydrogen_output.csv", delimiter = ",",
                    usecols = (2, 3, 5), skip_header = 1)

#get vars
vesc_n = hydro[:,[0][0]]
v_n = hydro[:,[1][0]]
k_n = hydro[:,[2][0]]


dv_n = v_n / vesc_n

pl.figure()
#plot histogram
n, bins, patches = pl.hist(dv_n, 15, normed=True, histtype="stepfilled")
pl.setp(patches, "facecolor", "b", "alpha", 0.75)
mean = np.mean(dv_n)
variance = np.var(dv_n)
sigma = np.sqrt(variance)
pl.plot(bins, pl.normpdf(bins,mean,sigma), "k--")
pl.title("Hydrogen Impact Velocity")
pl.xlabel("Impact Velocity / Escape Velocity")
pl.savefig("hydrogen_hist_0.png")

#plot histogram
pl.figure()
n, bins, patches = pl.hist(dv_n, 30, normed=True, histtype="stepfilled")
pl.setp(patches, "facecolor", "b", "alpha", 0.75)
mean = np.mean(dv_n)
variance = np.var(dv_n)
sigma = np.sqrt(variance)
pl.plot(bins, pl.normpdf(bins,mean,sigma), "k--")
pl.title("Hydrogen Impact Velocity")
pl.xlabel("Impact Velocity / Escape Velocity")
pl.savefig("hydrogen_hist_1.png")

#plot histogram
pl.figure()
n, bins, patches = pl.hist(dv_n, 40, normed=True, histtype="stepfilled")
pl.setp(patches, "facecolor", "b", "alpha", 0.75)
mean = np.mean(dv_n)
variance = np.var(dv_n)
sigma = np.sqrt(variance)
pl.plot(bins, pl.normpdf(bins,mean,sigma), "k--")
pl.title("Hydrogen Impact Velocity")
pl.xlabel("Impact Velocity / Escape Velocity")
pl.savefig("hydrogen_hist_2.png")

pl.show()





