#saif charaniya
#plot a scatter plot of the ratio v_imp / v_esc
#for both hydrogen shell and normal systems
#can be configured and customized below

import numpy as np
import pylab as pl

#read file
normal = np.genfromtxt("../final_normal_output.csv", delimiter = ",",
                    usecols = (2, 3, 5), skip_header = 1)
hydrogen = np.genfromtxt("../final_hydrogen_output.csv", delimiter = ",",
                    usecols = (2, 3, 5), skip_header = 1)


#get vars
vesc_n = normal[:,[0][0]]
v_n = normal[:,[1][0]]
k_n = normal[:,[2][0]]
vesc_h = hydrogen[:,[0][0]]
v_h = hydrogen[:,[1][0]]
k_h = hydrogen[:,[2][0]]

dv_n = v_n / vesc_n
dv_h = v_h / vesc_h


final_v = np.append(dv_n, dv_h)

final_k = np.append(k_n, k_h)

#plot scatter k_mean
pl.figure()
pl.title("Impact Velocity Scatter Plot")
pl.plot(final_k, final_v, "ro")
pl.xlabel("$K_{mean}$")
pl.ylabel("$V_{imp}$ / $V_{esc}$")
#pl.savefig("scatter_plot.png")
pl.show()
