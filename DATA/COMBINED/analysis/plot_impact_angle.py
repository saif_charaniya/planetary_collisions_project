#saif charaniya
#plot dx and dv against k using csv file

import numpy as np
import pylab as pl

#read file
normal = np.genfromtxt("../final_normal_output.csv", delimiter = ",",
                    usecols = (4), skip_header = 1)
hydrogen = np.genfromtxt("../final_hydrogen_output.csv", delimiter = ",",
                    usecols = (4), skip_header = 1)


#get vars
a_n = normal
a_h = hydrogen

#convert to degrees
a_n = a_n * 57.2957795
a_h = a_h * 57.2957795

#plot scatter k_mean
pl.figure()
pl.title("Impact Velocity Scatter Plot")
pl.plot(a_n, "ro")
pl.plot(a_h, "bo")
pl.savefig("anlge_scatter_plot.png")
pl.show()
