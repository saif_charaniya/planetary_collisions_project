#saif charaniya
#plot dx and dv against k using csv file

import numpy as np
import pylab as pl

#read file
normal = np.genfromtxt("../final_normal_output.csv", delimiter = ",",
                    usecols = (2, 3, 5), skip_header = 1)
hydrogen = np.genfromtxt("../final_hydrogen_output.csv", delimiter = ",",
                    usecols = (2, 3, 5), skip_header = 1)
merger = np.genfromtxt("single_collisions,csv", delimiter = ",")


#get vars
vesc_n = normal[:,[0][0]]
v_n = normal[:,[1][0]]
k_n = normal[:,[2][0]]
vesc_h = hydrogen[:,[0][0]]
v_h = hydrogen[:,[1][0]]
k_h = hydrogen[:,[2][0]]
v_m = merger[:,[0][0]]
k_m = merger[:,[1][0]]

##pl.figure()
##pl.plot(k_m, "ro")
##pl.show()


dv_n = v_n / vesc_n
dv_h = v_h / vesc_h

#plot scatter k_mean
pl.figure()
pl.title("Impact Velocity Scatter")
pl.plot(k_n, dv_n, "ro")
pl.xlabel("K")
pl.ylabel("$V_{imp}/V_{esc}$")
pl.savefig("normal.png")
##
##pl.figure()
##pl.title("Hydrogen Systems")
##pl.plot(k_h, dv_h, "bo")
##pl.xlabel("K")
##pl.ylabel("Impact Velocity / Escape Velocity")
##pl.savefig("hydrogen.png")
##
##pl.figure()
##pl.title("Impact Velocity Scatter")
##pl.plot(k_n, dv_n, "ro", label="Normal")
##pl.plot(k_h, dv_h, "bo", label="H")
##pl.xlabel("K")
##pl.ylabel("$V_{imp}/V_{esc}$")
##pl.legend(loc=0)
##pl.savefig("combined.png")


pl.show()
print ("done")
