#saif charaniya
#used to combine the 3 csv files together and keeping 1 line of data
#per system (ie removing duplicates)

import numpy as np

#file names
f1 = "normal_systems_output.csv"
f2 = "normal_systems_output_01.csv"
f3 = "normal_systems_output_02.csv"

#read files
data1 = np.genfromtxt(f1, delimiter = ",", skip_header = 1)
data2 = np.genfromtxt(f2, delimiter = ",", skip_header = 1)
data3 = np.genfromtxt(f3, delimiter = ",", skip_header = 1)

#seperate the system numbers
system1 = np.array(data1[:,[0][0]], dtype = int)
system2 = np.array(data2[:,[0][0]], dtype = int)
system3 = np.array(data3[:,[0][0]], dtype = int)

#array to hold final data output
final_data = []
final_systems_nums = []

#iterate through the system numbers
for i in range(len(system1)):
    s = system1[i]
    try:
        final_systems_nums.index(s)

        #do nothing
    except ValueError:
        # add system
        final_systems_nums.append(s)
        final_data.append(data1[i])
        
for i in range(len(system2)):
    s = system2[i]
    try:
        final_systems_nums.index(s)

        #do nothing
    except ValueError:
        # add system
        final_systems_nums.append(s)
        final_data.append(data2[i])

for i in range(len(system3)):
    s = system3[i]
    try:
        final_systems_nums.index(s)

        #do nothing
    except ValueError:
        # add system
        final_systems_nums.append(s)
        final_data.append(data3[i])

#collect and save final_data to a new csv file
ff = "final_normal_output.csv"
save_file_header = "system_number,collision_time,v_esc,v,angle,k_mean,k_min,k_std\
,m1,m2,m3,m4,m5,m6,m7,sma1,sma2,sma3,sma4,sma5,sma6,sma7"
np.savetxt(ff, final_data, delimiter = ",",\
           header = save_file_header)
print ("done")
