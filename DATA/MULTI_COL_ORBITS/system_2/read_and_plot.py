#saif charaniya
#read an orbit output file from rebound
#and plot the ecc and semi-major axis for each planet

import numpy as np
import pylab as pl

file_name = "orbits_system_2"

contents = np.genfromtxt(file_name, usecols=(0,1,2,3,4,5,6))

#vars
t = contents[:,[0][0]]
pn = contents[:,[1][0]]
sm = contents[:,[2][0]]
ec = contents[:,[3][0]]
inc = contents[:,[4][0]]
Omega = contents[:,[5][0]]
omega = contents[:,[6][0]]

#planet vars
p1 = []
p2 = []
p3 = []
p4 = []
p5 = []
p6 = []
p7 = []

all_p = [p1, p2, p3, p4, p5, p6, p7]
#extraction
for k in range(1, 8):
    where = np.where(pn == k)[0]
    all_p[k - 1].append(t[where]) #time
    all_p[k - 1].append(sm[where]) #semi-major axis
    all_p[k - 1].append(ec[where]) #eccentricity
    all_p[k - 1].append(inc[where]) #inc
    all_p[k - 1].append(Omega[where]) #ascending node
    all_p[k - 1].append(omega[where]) #argument of periapsis
    
#plot semi_major
ax3 = pl.figure()
ax3 = pl.subplot(111)
pl.title("Semi Major Axis")
for i in range(7):
    ax3.plot(all_p[i][0], all_p[i][1], label="p"+str(i+1))
pl.xlabel("Time [yr]")
pl.ylabel("Semi-Major Axis [AU]")
ax3.set_xscale("log")
#pl.legend(loc=0)
box = ax3.get_position()
ax3.set_position([box.x0, box.y0, box.width * 0.8, box.height])
ax3.legend(loc='center left', bbox_to_anchor=(1, 0.5))
#pl.savefig("sm.png")
pl.show()

fig = pl.figure()
ax = pl.subplot(111)
pl.title("Eccentricity")
for i in range(7):
    ax.plot(all_p[i][0], all_p[i][2], label="p"+str(i+1))
pl.xlabel("Time [yr]")
box = ax.get_position()
ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
ax.set_xscale("log")
ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
pl.ylabel("Eccentricity")
#pl.legend(loc=0)
#pl.savefig("ecc.png")
#pl.show()

ax2 = pl.figure()
ax2 = pl.subplot(111)
pl.title("Inclination")
for i in range(7):
    ax2.plot(all_p[i][0], all_p[i][3], label="p"+str(i+1))
pl.xlabel("Time [yr]")
ax2.set_xscale("log")
pl.ylabel("Inclination")
#pl.legend(loc=0)
box = ax2.get_position()
ax2.set_position([box.x0, box.y0, box.width * 0.8, box.height])
ax2.legend(loc='center left', bbox_to_anchor=(1, 0.5))
pl.show()

##pl.savefig("inc.png")
##
##ax4 = pl.figure()
##ax4 = pl.subplot(111)
##pl.title("Argument of Periapsis $\omega$")
##for i in range(7):
##    ax4.plot(all_p[i][0][:800], all_p[i][5][:800], label="planet "+str(i+1))
##pl.xlabel("Time [yr]")
##pl.ylabel("$\omega$")
###pl.legend(loc=0)
##box = ax4.get_position()
##ax4.set_position([box.x0, box.y0, box.width * 0.8, box.height])
### Put a legend to the right of the current axis
##ax4.legend(loc='center left', bbox_to_anchor=(1, 0.5))
##pl.savefig("omega.png")

pl.show()
    
