#saif charaniya
#read an orbit output file from rebound
#and plot the ecc and semi-major axis for each planet

import numpy as np
import pylab as pl

file_name = "orbits_system_10"

contents = np.genfromtxt(file_name, usecols=(0,1,2,3))

#vars
t = contents[:,[0][0]]
pn = contents[:,[1][0]]
sm = contents[:,[2][0]]
ec = contents[:,[3][0]]

#planet vars
p1 = []
p2 = []
p3 = []
p4 = []
p5 = []
p6 = []
p7 = []

all_p = [p1, p2, p3, p4, p5, p6, p7]
#extraction
for k in range(1, 8):
    where = np.where(pn == k)[0]
    all_p[k - 1].append(t[where]) #time
    all_p[k - 1].append(sm[where]) #semi-major axis
    all_p[k - 1].append(ec[where]) #eccentricity
    
#plot semi_major
pl.figure()
pl.title("Semi Major Axis")
for i in range(7):
    pl.plot(all_p[i][0], all_p[i][1], label="planet "+str(i+1))
pl.xlabel("Time [yr]")
pl.ylabel("Semi-Major Axis [AU]")
pl.legend(loc=0)
pl.savefig("sm.png")
pl.show()

pl.figure()
pl.title("Eccentricity")
for i in range(7):
    pl.plot(all_p[i][0], all_p[i][2], label="planet "+str(i+1))
pl.xlabel("Time [yr]")
pl.ylabel("Eccentricity")
pl.legend(loc=0)
pl.savefig("ecc.png")
pl.show()

    
