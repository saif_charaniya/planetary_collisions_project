\documentclass[twoside]{article}
\usepackage{indentfirst}



% ------
% Fonts and typesetting settings
\usepackage[sc]{mathpazo}
\usepackage[T1]{fontenc}
\linespread{1.05} % Palatino needs more space between lines
\usepackage{microtype}

% ------
% Page layout
\usepackage[hmarginratio=1:1,top=32mm,columnsep=22pt]{geometry}
\usepackage[font=it]{caption}
\usepackage{paralist}
\usepackage{multicol}

% ------
% Lettrines
\usepackage{lettrine}


% ------
% Abstract
\usepackage{abstract}
	\renewcommand{\abstractnamefont}{\normalfont\bfseries}
	%\renewcommand{\abstracttextfont}{\normalfont\small\itshape}


% ------
% Titling (section/subsection)
\usepackage{titlesec}
\renewcommand\thesection{\arabic{section}}
\titleformat{\section}[block]{\large\scshape\centering}{\thesection.}{1em}{}
\titleformat{\subsection}[block]{\large\scshape\centering}{\thesubsection}}{}{}
\titleformat{\subsection}{\normalfont}{\thesubsection}{1em}{}

\setcounter{secnumdepth}{2} 


% ------
% Header/footer
\usepackage{fancyhdr}
	\renewcommand{\thefootnote}{\arabic{footnote}}
	\pagestyle{fancy}
	\fancyhead{}
	\fancyfoot{}
	\fancyhead[C]{AST425Y1 $\bullet$ Final Paper $\bullet$ Tuesday April 7, 2015}
	\fancyfoot[RO,LE]{\thepage}


% ------
% Clickable URLs (optional)
\usepackage{hyperref}
\usepackage{graphicx}

% ------
% Maketitle metadata
\title{\vspace{-15mm}%
	\fontsize{24pt}{10pt}\selectfont
	\textbf{Understanding Planetary Evolution through simulation of Planet collisions}
	}	
\author{%
	\large
	\textsc{Saif Charaniya}\footnote{Department of Astronomy and Astrophysics, University of Toronto, 50 St. George Street, Toronto, Ontario, M56 3H4 \href{mailto:saif.charaniya@mail.utoronto.ca}{Email}} \\[2mm]
	\textsc{Yanqin Wu}\footnote{Department of Astronomy and Astrophysics, University of Toronto, 50 St. George Street, Toronto, Ontario, M5S 3H4 \href{mailto:wu@astro.utoronto.ca}{Email}}
	\vspace{-5mm}
	}
\date{}

\setlength{\abovecaptionskip}{12pt}
\setlength{\belowcaptionskip}{12pt} 


%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

\maketitle
\thispagestyle{fancy}

\begin{abstract}
We study the impact parameters of planetary collisions by Integrating over 6000 artificial planetary systems using Rebound's 15th order non symplectic integrator.  We find the ratio between impact velocity and escape velocity, \ensuremath{v_{imp} / v_{esc}}, to equal 1 for the initial collisions and to be increasing with the mutual hill radius, or K spacing, of the planetary system.  We predict that the initial collisions result in the merging of both planets involved.  Studying later collisions in the system we find that the ratio \ensuremath{v_{imp} / v_{esc}} increases as the number of collisions increases.  We find this means that later collisions result in violent planetary encounters as observed from the orbital parameters of the planets.  Final stable systems are observed to contain one to two planets of size 15 - 40 $M_{E}$ in secular evolution.
\end{abstract}

\begin{multicols*}{2}
\section{Introduction}
%\lettrine[nindent=0em,lines=3]{I} 
\par Since the confirmed detection of the first extra-solar planet, or exoplanet, PSR B1257+12 in 1992 (Wolszczan 1992) we have come to know of over 1700 confirmed extra solar planets, largely through NASA's Kepler mission. Even more interestingly, over 450 multiplanetary systems have been confirmed including the largest, which is a seven planet system  (Lovis 2011).  Indeed multiplanetary systems can help us unlock the many mysteries surrounding the evolution of planetary systems.  Here we will look into conducting simulations of these systems in order to determine a velocity distribution from impact parameters during planet collisions.  These simulations can help us predict outcomes of planetary collisions and help us understand the reasons behind tight primordial spacing and planet evolution.
\par One of the most interesting observation from the Kepler systems has been tightly packed  orbits of multi-planetary systems.  These systems consist of several super Earth size planets   closely orbiting their hosts and in most cases are within a few percent of an AU to each other.  These systems have been found to be dynamically cold and with low eccentricities and inclinations.  One such proposal is that perhaps these systems have evolved through stages of instability leading to their packed orbits.  This poses an interesting contradiction, where on the one hand we find that these systems are very simple and delicate in order to avoid catastrophic behaviour and on the other hand we believe that they have formed from very disruptive situations and complex planetary evolution.
\par Motivated to learn more about the primordial planetary spacing and its evolution we look into finding what happens to these systems when they no longer remain stable.  In this paper we outline the methods and results of our simulations of planetary collisions and present an analysis of the impact parameters of these close encounters.  Planetary collisions is a field which has not been thoroughly studied.  Chambers et al (1996) studied the stability time and planetary collisions of three planet systems, but did not take into account our current exoplanet evidence of compact spacing.  Similarily Hands et al (2014) studied the migration of planets in tightly packed Kepler systems but did not look into any impact dynamics.  Thus the most relevant research we use as a comparison is from Agnor (2004) and Asphaug (2009) who both, while studying the asteroids collisions in our solar system, conducted simulations of colliding planetary embryos.  Their mass accretion efficiency results are of most interest to us, as is the work of Marcus et al (2009) who conducted a similar experiments as Asphaug in order to determine planet formation through collisions of planetary embryos.

\section{Data Organization and Integration}
\par To simulate planetary collisions we use a catalogue of over 8000 artificial planetary systems created by Bonan Pu.  Each system contains a solar mass host star with seven planets.  Each planet mass is chosen from a normal distributing between \ensuremath{3.0 M_{E} \geq m \leq 9.0M_{E}}, where \ensuremath{M_{E}} is the mass of the Earth.  Planet radius is calculated using the simple mass-radius relation below, where \ensuremath{R_{E}} is the radius of the Earth.
\begin{equation}
 R_{p} = \frac{R_{E}M_{p}}{3M_{E}}
\end{equation}
Each system begins with the innermost planet at a distance of 0.1AU from the host star followed by the remaining six planets not spaced evenly but rather their spacing determined through a normal distribution of K spacing, and a given \ensuremath{K_{mean}} representing the entire system's spacing.  K spacing is a dimension less quantity used to desribe the primodrial tightly packed planetary systems.  Systems with $k_{mean} \leq 10$ have been found to reach unstability in under $10^7$ yrs (PU 2015) which is reflected in the systems in our catalogue.  The integration of the systems begins with all the planets having an initial eccentricity of zero and normal spread in initial inclination.  As described by Chambers (1996) the inclination plays a key role in the stability time of the system, and as expected we found that pertabations in the inclinations lead to uncorrelated changes in stability time when looking at the same planetary system.
\par Simulations are done with IAS15 a 15 th order non symplectic integrator found in the Rebound library created by Hanno Rein and David S. Spiegel (2014) which is based off of a modified Runge Kutta integrator.  Rebound uses an adaptive time step which allows us greater precision when integrating close encounters.  Adapting the open source examples from Rebound we were able to create the dessired environment for the simulations.  As required, our implementation can record many elements of the systems including planet orbital parameters, postions and velocities, and encounter times.  We use IAS15 to describe a collision as the point when two planets come within their hill radius which we define simply as the planet's radius.
\par During the integrations done to study the first collisions in the system we set Rebound to stop integrating approximately 10 years in time after the first collision.  This was done to save time and speed up the analysis.  For the integrations done to study later collisions, we set Rebound to assume that every collision in the system consisting of two or more planets would result in a complete merger and 100\% mass accretion.  More details on the rationale behind this is in Section 4.  The resulting planet from the collision would maintain the same density.  Further we exited the integration after the sixth collisions, meaning that we would finish with one remaining planet in the system.
\par Integrations of planetary systems were completed on the Sunnyvale Beowulf cluster located at the Canadian Institute of Theoretical Astrophysics (CITA).  Through Sunnyvale we were able to conduct over six hundred thousand hours of integration using Rebound.  The only limitation we found was the the speed and allotted maximum wall-time of 48 hours, meaning that with the heavy duty work of Rebound's IAS15 integrator, planetary systems could only be integrated for approximately 4.5 to 5 million years.

\section{Primary Collisions}
\par Primary collisions are of great interest to us as we look to understand the dynamics behind planetary collisions, hence we aim at resolving a impact velocity distribution from the collisions.  We begin by integrating normal systems which are described above and which follow the mass-radius relationship of Equation 1.  In this setup we examine the heliocentric coordinates of every planet as well as its velocity vectors with periodic outputs through Rebound every 10 yrs in time and during close encounters.  After conducting integrations of 2434 of these systems we moved on next to better understand the effects of radius and density.  To do this we looked at Hydrogen shell planets which we defined as having twice the radius of the normal systems described above, and hence half the density.  Our motivation for studying these types of planets stems from recent discoveries of Hydrogen atmosphere exoplanets through H$\alpha$ emission as is the case for HD189733b (Christie 2013).  We integrated 1291 of the previous normal systems to observe how the impact parameters may change.  A combined scatter plot showing the results and spread in velocities is shown in Figure 1 below.
\par Right away we notice that the change in density and radius does not change the outcomes of the collision.  This is what we expected because our integration of the Hydrogen shell systems does not take into account how a Hydrogen atmosphere may change the collision dynamics because Rebound is treating our planets as essential point masses.
\begin{Figure}
 \includegraphics[width=\linewidth]{combined.png}
 \captionof{figure}{A scatter plot showing the spread in the ratio of impact velocity and escape velocity of both normal systems (in red) and Hydrogen shell systems (in blue).  We find that the spread in impact velocities is statistically similar to the normal systems.}
\end{Figure}
Thus the fact that we see the Hydrogen Shell  planets behaving the same way as the normal systems gives us more confidence in the integration dynamics of the close encounters for our simple case of point particles.  Figure 1 also shows us that the mass-radius relationship also does not play an important part in determining the collision dynamics, and that we can expect the same results with using another mass radius relationship other than Equation 1 above, such as for example one for large Jupiter size planets.  Because of the non dependance of the mass-radius relation we combine the two sets of systems for the remained of the analysis on the primary collision.
\par We now move on to the study of impact angles, where we can find the impact angle, $\Theta$, of the collision using simple vector calculus described below in Equation 2, where \textbf{v} is the impact velocity and $\Delta$\textbf{r} is differnce between the position vectors of the planets.
\begin{equation}
 \Theta = \arccos ( \frac{\Delta \textbf{r} \cdot \textbf{v}}{\left \|  \Delta\textbf{r} \right \| \left \| \textbf{v} \right \|} )
\end{equation}
We find that the impact angles of collisions follow a normal distribution centred at $\Theta$ = 45$^{\circ}$ as seen in Figure 2 below.  This is in accordance with the results of Wang (2006) and Shoemaker (1962) who describe the probability of the impact angle as equal to \ensuremath{\sin \left ( 2\Theta  \right )\delta \Theta}. 
\begin{Figure}
 \includegraphics[width=\linewidth]{angle_hist.png}
 \captionof{figure}{Distribution of impact angles, as calculated using Equation 2, are normal distributed and centered at 45$^{\circ}$ as expected.}
\end{Figure}
\par To get a better understanding of the aftermath of planetary collisions we shifted our attention to understanding the ratio of impact velocity and escape velocity.  Starting with a histogram of the spread in velocities from Figure 1 we found there to be a sharp peak centred at \ensuremath{v_{imp} / v_{esc}} = 1, which is shown in Figure 3 below.  
\begin{Figure}
 \includegraphics[width=\linewidth]{histogram.png}
 \captionof{figure}{Histogram of the combined impact velocity and escape velocity ratio found to be centered at 1.  We place more confidence in the median as determined through the large spread in values in Figure 1.}
\end{Figure}
Comparing this result to Asphaug (2009) and Agnor (2004) study on the impacts of asteroids and large planetary embryos we find that a ratio near unity for the impact velocity and escape velocity translates to a near 100 percent mass accretion efficiency from the collision.  This means that the most likely result of the primary collision in a tightly packed planetary system is that the two planets would merge entirely.  This also shows us that near the point of close encounter the gravity and influence between the two planets takes over from the influence of the host star and it is this influence which is the primary force behind the impact velocity and not the planet's external forces or velocities when they are infinitely separated, for example at t =0. 
\par Lastly we discuss the relationship between the k spacing, or \ensuremath{k_{mean}}, and the impact velocity ratio.  From Figure 1 above, it may seem that the there is no correlation, especially because of the large spread in values for the ratio \ensuremath{v_{imp} / v_{esc}}.  To better analyze the data we use a running binning method with a bin of width $k_{mean}$ = 1 starting at $k_{mean}$ =  6.5 and ending at $k_{mean}$ = 10.0.  Using the running bin method we are able to break the interval of $k_{mean}$ = 3.5 into seven bins each of half the bin width.  This results in a much clearer picture of the relationship between k spacing and the impact velocity, which can be found in Figure 4.  
\begin{Figure}
 \includegraphics[width=\linewidth]{binning.png}
 \captionof{figure}{Using the power of data analytics we can reduce the scatter plot from Figure 1 into a much clearer picture showing the intrinsic relationship between the k spacing, or \ensuremath{k_{mean}}, and the ratio \ensuremath{v_{imp} / v_{esc}}.}
\end{Figure}
Through the binning process we find a linear relationship between the k spacing and the ratio of the impact velocity and escape velocity.  After conducting a least squares linear fit we find for the median the relationship can be written as \ensuremath{v_{imp}/v_{esc} = (0.96 \pm 0.4 \cdot 10^1) + (0.086 \pm 0.5 ) \cdot k_{mean}} with a root mean square value 0f 1.22 while for the mean can be written as \ensuremath{v_{imp}/v_{esc} = (0.98 \pm 0.4 \cdot 10^1) + (0.014 \pm 0.5 ) \cdot k_{mean}} with a root mean square value of 1.29.  
When comparing this to our histogram in Figure 3, we see that the average ratio of \ensuremath{v_{imp}/v_{esc}} is now diverging away from its value of 1.  Comparing this again to the results of Agnor and Asphaug we find that the primary collisions in these planetary systems have mass accretion efficiency of between 80$\%$ to 100$\%$ depending on the system's k spacing.

\section{Merging Collisions}
\subsection{Dynamics of Later Collisions}
\par In section 3 above we found that primary collisions largely result in complete mergers of the two impacting planets.  This part of the study aims at using the same dynamical techniques to understand what happens in later collisions.   Consider our artificial planetary systems of seven planets.  In these systems we can have at most six collisions between planets until what remains is a single planet orbiting its host star.  To do this simulation using Rebound we assume that all collisions taking place after the first collision are complete mergers just like the primary collisions.  During these mergers we combine the masses of the two planets, recalculate the radius using Equation 1 and abandon one of the two planets while continuing the integration.  This ensures that every planet formed through a merger maintains the same density as before the collision and effectively also has the same escape velocity for the remainder of the integration. Again we allow Rebound to output the heliocentric coordinates and velocity components in order to conduct a similar analysis as in the previous section.  The merging integration was run on 1655 systems from the catalogue which provided us with data on 5441 collisions taking place, split between the first through fifth collision in the system.  Since these systems move inherently towards long term stabilization as the number of planets decrease through collisions, we found that in all of our planetary systems there was no sixth collision during the integration.  This does not necessarily imply that these systems transitioned towards full stabilization but rather that as we became left with two planets in the systems we would require much more computing time and integration time to observe the next and final possible collision.
\par We first observed that as we move into later collisions the ratio \ensuremath{v_{imp}/v_{esc}} increases drastically as can be seen in Figure 5, and peaks at a mean value of approximately 1.40.  This is a significant leap from our findings on primary collisions.  Comparing once more to Agnor and Asphaug we find that in the fifth collision our accretion efficiency has dropped to around 20&\%&, significantly lower than in the primary collisions and no where near the assumption of a complete merger.  Further the increases in impact velocity as we move to the later collisions tells us that these planetary encounters would have consisted of chaotic, violent and disruptive events, no where near computationally understandable with our software. 
\begin{Figure}
 \includegraphics[width=\linewidth]{linear_multi.png}
 \captionof{figure}{Plot of the impact velocities in later collisions. Later collisions are found to be violent and chaotic thus not allowing us to take them as complete mergers as we did for the primary collision in the planetary systems.}
\end{Figure}
\subsection{Orbital Parameters}
\par Lastly we reexamine 20 planetary systems with Rebound set to output orbital parameters periodically and during close encounters. Below we discuss two systems, system 10 and system 2, which both give us great insight into what happens during the evolution of these systems.  
\begin{Figure}
\includegraphics[width=\linewidth]{sm_p3.png}
 \captionof{figure}{The semi major axis for System 10.  Notice how planet 3 disappears, as we find it is headed for a collision with the host star.} 
\end{Figure}
\par System 10, which has a plot of its semi major axis in Figure 6, undergoes four quick collisions by t = 85 000; leaving three planets remaining in the system.  Planet number 3 in the plot, can be seen developing a growing semi major axis, then disappearing from the plot.  When taking a closer look at the eccentricity of planet 3 and calculating its periapsis we find that the planet is on its way to a collision with the host star, but before this can happen our integration breaks down and the planet is simply removed from the system.  This shows us two important observations, firstly that planet collisions are not the only outcomes in these systems and that there is always the possibility that later collisions may result in planets colliding with the host star.  Secondly it shows us that to understand such a collision we would require a more sophisticated computational approach.
\par System 2, unlike System 10 has no problems during its integration, and hence allowed us to observer its orbital parameters more closely.  We first find that the primary collision occurs between planet 7 and planet 2.  This was also the case in System 10 above, and in fact a majority of the 20 systems we studied, where the outer most planet would collide with the one of the innermost planets.  After this collision planet 7 and planet 2 would merge into planet 2 which would go on to be one of two planets remaining in the system with a total mass of approximately 16$M_{E}$.  This collision can be seen quite frantically by the jump in planet 2's position as seen in Figure 7.
\begin{Figure}
\includegraphics[width=\linewidth]{figure_1.png}
 \captionof{figure}{The semi major axis for System 2, with the planet 2 and planet 1 as the remaining planets in the system.} 
\end{Figure}
The next four collisions results in the remaining five planets merging into a final state into planet 1 and achieving a final mass of approximately 33$M_{E}$ or twice the mass of Neptune.  The five collisions of the system take place in just under the t = 0.5 myr mark, and what remains is the two inner planets with Neptune size masses and stabilized semi major axises.
\par More interesting than the semi major axis are the plots of eccentricity and inclination of System 2 shown in Figure 8 and Figure 9 respectively.  Most notably we find that these collisions are very dynamical.  This tells us that the evolution of these systems are very complex.  After t = 0.5 myr, we see the remaining two planets undergoing a long period of secular evolution as found in their oscillating eccentricities and inclinations which occurs through their exchanges of angular momentum (Rodriguez 2012).  This system remains in this state until the t = 2.5 myr when the integration is stopped.     
\begin{Figure}
\includegraphics[width=\linewidth]{figure_2.png}
 \captionof{figure}{The eccentricity for System 2.  Notice how the eccentricities of planet 1 and planet 2 osscilate through secular evolution.  The secular evolutuion would remain till t = 2.5 myr.} 
\end{Figure}
\begin{Figure}
\includegraphics[width=\linewidth]{figure_3.png}
 \captionof{figure}{The inclination of System 2.  Notice how during collision times the inclination becomes chaotic, showing us there is more complexity behind the dynamics of planetary evolution.} 
\end{Figure}


\section{Summary and Conclusion}
\par In this paper we have presented the results of integrating over 6000 artificial planetary systems using Rebound's high precision 15 th order IAS15 integrator library, and conducting over 600 000 hours of computational work on the CITA Sunnyvale cluster in a 4 month time line.  Our results shed important light on the dynamics of close encounters in planetary systems and in the understanding of planetary evolution.  
\par We found that primordial compact systems, as discovered by the Kepler space mission, can evolve to more stabilized systems which we have also observed through exoplanet study.  Our results show that tightly pack super Earth sized planets can evolve through mass accretion from close encounters into Neptune and Super Neptune size.   We found that the primary collision ratio of impact velocity and escape velocity to be highly peaked at a value of 1.0.  Comparing this result to previous studies of mass accretion we expect the primary, or first collision in the tightly packed system, to result in nearly 100$\%$ mass accretion efficiency.   Further we find that in these tightly packed systems the first collision occurs between an inner and outer an planet.  
\par We have learned that later collisions cannot be treated as complete mergers but require a more sophisticated computational requirement to understand the effects of mass accretion which can be anywhere from 20$\%$ to 60$\%$ depending on the impact parameters.  We have shown that during the evolutionary stages of these planetary systems there is much that can be learned from understanding the changes in orbital parameters, specifically the eccentricity and inclination, and that the planetary collisions are not the only outcomes, as we can also have collisions with the host star.  
\par Further areas of study can take a closer look in to the areas of orbital parameters and mass accretion evolution and can extend the integrations to understand long term stability of planets as well.  There is clearly much to be learned by examining how these parameters affect planetary collisions and how they play a role in the evolution of these systems both during chaotic collision stages and during periods of secular evolution and increased stabilization.  Understanding these dynamics in the future can lead to new knowledge and theories on planet formation and can help us also better understand the formation and evolution of our own solar system.

\newlength\listindent
\setlength\listindent{50pt}
\newlength\labellength

\let\oldthebibliography\thebibliography
\let\oldendthebibliography\endthebibliography
\renewenvironment{thebibliography}[1]
 {%
  \settowidth{\labellength}{#1}\addtolength{\labellength}{1em}%
  \oldthebibliography{#1}%
  \parshape=2%
    \labellength \linewidth%
    \listindent \dimexpr\linewidth-\listindent+\labellength\relax%
  }{%
   \oldendthebibliography%
  }

\makeatletter
\renewcommand\@biblabel[1]{}
\makeatother
\begin{thebibliography}{9}
    \bibitem{}{Agnor, C., & Asphaug, E. 2004, ApJ, 613, L157}
    \bibitem{}{Asphaug, E. 2009, Annual Review of Earth and Planetary Sciences, 37, 413}
    \bibitem{}{Chambers, J. E., Wetherill, G. W., & Boss, A. P. 1996, Icarus, 119, 261}
    \bibitem{}{Christie, D., Arras, P., Li, Z.-Y. 2013, ApJ, 772, 144}
    \bibitem{}{Hands, T. O., Alexander, R.D., Dehnen, W. 2014, Royal Astronomical Society, 445, 749}
    \bibitem{}{Lovis, C., Segransan, D., Mayor, M., et al. 2011, A&A, 528, A112}
    \bibitem{}{Marcus, R. A., Stewart, S. T., Sasselov, D., & Hernquist, L. 2009, ApJ, 700, L118}
    \bibitem{}{Pu, B., Wu, Y. 2015, arXiv:1502.05449}
    \bibitem{}{Rein, H., Spiegel, D.S. 2014, arXiv:1409.4779}
    \bibitem{}{Shoemaker, E.M 1962, in Physics and Astronomy of the Moon, ed. Z. Kopal (New York Academic), 283}
    \bibitem{}{Wolszczan, A. Frail, D. A. 1992, Nature, 355, 145}
    \bibitem{}{Wang, L.P., Franklin, C.N., Ayala, O., et al. 2006, J. Atmos. Sci, 63, 881}
\end{thebibliography}

\end{multicols*}
\end{document}
