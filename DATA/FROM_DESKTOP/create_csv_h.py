#saif charaniya
#create csv file for "collisions_sc_normal.txt"

#import statements
import numpy as np

def period_to_sma(period):    
    t = period * 24 * 60 * 60
    
    r = ((t ** 2 * G * MSUN) / (4 * np.pi ** 2)) ** (1.0 / 3.0)
    return r / AU

#constants
G = 6.67e-11
MSUN = 1.9891e30
AU = 149597871000

#read files
bonan_file = np.genfromtxt("Bonan_FlatCoplanar.csv", delimiter = ",", \
skip_header = 3, usecols = (3, 4, 5, 7, 15, 16, 17, 18, 19, 20, 21, 28, 29, 30,\
31, 32, 33, 34))

collision_file = np.genfromtxt("collisions_hydrogen.txt")

sorted_file = np.genfromtxt("sorted.csv")

#set vars
system_num = collision_file[:,[0][0]]
system_num = np.array(system_num, dtype=int)
time = collision_file[:,[1][0]]
x = collision_file[:,[2][0]]
y = collision_file[:,[3][0]]
z = collision_file[:,[4][0]]
vx = collision_file[:,[5][0]]
vy = collision_file[:,[6][0]]
vz = collision_file[:,[7][0]]

k_mean = bonan_file[:,[0][0]][system_num - 1]
k_min = bonan_file[:,[1][0]][system_num - 1]
k_std = bonan_file[:,[2][0]][system_num - 1]

m1 = sorted_file[:,[1][0]][system_num - 1]
m2 = sorted_file[:,[2][0]][system_num - 1]
m3 = sorted_file[:,[3][0]][system_num - 1]
m4 = sorted_file[:,[4][0]][system_num - 1]
m5 = sorted_file[:,[5][0]][system_num - 1]
m6 = sorted_file[:,[6][0]][system_num - 1]
m7 = sorted_file[:,[7][0]][system_num - 1]
sma1 = sorted_file[:,[8][0]][system_num - 1]
sma2 = sorted_file[:,[9][0]][system_num - 1]
sma3 = sorted_file[:,[10][0]][system_num - 1]
sma4 = sorted_file[:,[11][0]][system_num - 1]
sma5 = sorted_file[:,[12][0]][system_num - 1]
sma6 = sorted_file[:,[13][0]][system_num - 1]
sma7 = sorted_file[:,[14][0]][system_num - 1]

#convert au to km and 2p au /yr to km/s
vx = vx * 2 * np.pi * 4.7619 
vy = vy * 2 * np.pi * 4.7619
vz = vz * 2 * np.pi * 4.7619

x = x * 1.5e8
y = y * 1.5e8
z = z * 1.5e8

dot = (x * vx + y * vy + z * vz)

dx = np.sqrt(x ** 2 + y ** 2 + z ** 2) #au
dv = np.sqrt(vx ** 2 + vy ** 2 + vz ** 2) #km/s

cos_a = abs(dot / (dx * dv))
a = np.arccos(cos_a) #radians

v_esc = np.repeat([13.69262], len(x))

#save to csv file
save_file = "hydrogen_systems_output_02.csv"
save_file_header = "system_number,collision_time,v_esc,v,angle,k_mean,k_min,k_std\
,m1,m2,m3,m4,m5,m6,m7,sma1,sma2,sma3,sma4,sma5,sma6,sma7"
out_vals = (system_num, time, v_esc, dv, a, k_mean, k_min, k_std, m1, m2, m3, m4, m5,\
            m6, m7, sma1, sma2, sma3, sma4, sma5, sma6, sma7)
np.savetxt(save_file, np.column_stack(out_vals), delimiter = ",",\
           header = save_file_header)

print ("done")
